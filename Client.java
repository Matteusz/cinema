package Model;

public class Client {
    String firstName;
    String lastName;
    int age;

    public String getFullName(){
        return firstName + lastName;
    }

    public Client(String firstNam, String lastName, int age) {
        this.firstName = firstNam;
        this.lastName = lastName;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
