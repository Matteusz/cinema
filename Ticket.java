package Model;

import Model.Client;
import Model.FilmShow;

public class Ticket {
    private int id;
    private FilmShow movie;
    private Client client;

    public String getTicketInfo(){
        return id + " | " + movie.getFilmInfo() + " | " + client.getFullName();
    }

    public Ticket(int id, FilmShow movie, Client client) {
        this.id = id;
        this.movie = movie;
        this.client = client;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public FilmShow getMovie() {
        return movie;
    }

    public void setMovie(FilmShow movie) {
        this.movie = movie;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
