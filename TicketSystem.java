package System;

import Model.Client;
import Model.FilmShow;
import Model.Ticket;

public class TicketSystem {
    Ticket createTicket(FilmShow filmShow, Client client) {
        if (filmShow.getFreeSeats() == 0) {
            System.out.println("Brak wolnych miejsc na wskazany film");
            return null;
        } else if (client.getAge() < filmShow.getAgeRequired()) {
            System.out.println("Film dostępny dla osób powyżej: " + filmShow.getAgeRequired() + " lat ");
            return null;
        } else {
            int ticketID = filmShow.getMaxSeats() - filmShow.getFreeSeats() + 1;
            filmShow.setFreeSeats(filmShow.getFreeSeats() - 1);
            return new Ticket(ticketID, filmShow, client);
        }
    }

}
