package System;

import Model.Client;
import Model.FilmShow;
import Model.Ticket;

class CinemaSystem {
    public static void main(String[] args) {
        FilmShow filmShow = new FilmShow("Star wars", "Sci-fi", 120, 16,
                50, 20);
        Client client1 = new Client("Jan", "Kowalski", 23);
        TicketSystem ticketSystem = new TicketSystem();
        Ticket ticket1 = ticketSystem.createTicket(filmShow, client1);

        Client client2 = new Client("Dariusz", "Kruzel", 43);
        Ticket ticket2= ticketSystem.createTicket(filmShow, client2);

        System.out.println("Sprzedane bilety: ");
        if (ticket1 != null) {
            System.out.println(ticket1.getTicketInfo());
        }
        if (ticket2 != null) {
            System.out.println(ticket2.getTicketInfo());
        }
        System.out.println("Liczba pozostały miejsc: " + filmShow.getFreeSeats());
        System.out.println("Liczba sprzedanych biletów: " + (filmShow.getMaxSeats() - filmShow.getFreeSeats()));


        }


    }



